package com.fcfm.adaptadores

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import com.fcfm.adaptadores.adaptadores.Producto
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val productos= mutableListOf<Producto>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnGuardar.setOnClickListener {
            // creamos un nuevo item para para la lista
            val nombre = txtNombre.text
            // pasamos la información de los controles
            productos.add(
                Producto(
                    txtNombre.text.toString(),
                    txtDescripcion.text.toString(),
                    spCategoria.selectedItem.toString(),
                    spColor.selectedItem.toString()
            ))
            // pasamos la lista al activity
            val miIntent=Intent(this, ProductosActivity::class.java)
            var i = 0
            productos.forEach {
                miIntent.putExtra("nom$i", it.nombre)
                miIntent.putExtra("des$i", it.descriptcion)
                miIntent.putExtra("cat$i", it.categoria)
                miIntent.putExtra("col$i", it.color)
                i++
            }
            miIntent.putExtra("cantidad", i)
            startActivity(miIntent)
        }
        initSpinnerCategoria()
    }

    private fun initSpinnerCategoria(){
        val categorias= listOf(
            "Computo","Pantallas","Hogar","Oficina","Videojuegos"
        )

        val adaptador=ArrayAdapter<String>(
            this,android.R.layout.simple_spinner_dropdown_item,categorias
        )
        spCategoria.adapter=adaptador
    }
}

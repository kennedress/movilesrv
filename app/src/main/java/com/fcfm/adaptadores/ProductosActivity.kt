package com.fcfm.adaptadores

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import com.fcfm.adaptadores.adaptadores.Producto
import com.fcfm.adaptadores.adaptadores.ProductoAdapter
import kotlinx.android.synthetic.main.activity_productos.*

class ProductosActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_productos)
        val productos= mutableListOf<Producto>()
        /*for (i in 0..10){
            val producto=Producto(
                "Macbook $i",
                "Excelentes condiciones $i",
                "Computo",
                "Gris espacial"
            )
            productos.add(producto)
        }*/
        val limite = intent.getIntExtra("cantidad", 0) - 1
        for(i in 0 .. limite){
            productos.add(Producto(
                intent.getStringExtra("nom$i"),
                intent.getStringExtra("des$i"),
                intent.getStringExtra("cat$i"),
                intent.getStringExtra("col$i")
            ))
        }

        val adaptador=ProductoAdapter(productos)
        rvProductos.adapter=adaptador


        val divider=DividerItemDecoration(this,DividerItemDecoration.VERTICAL)

        rvProductos.addItemDecoration(divider)

    }

}

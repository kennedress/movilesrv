package com.fcfm.adaptadores.adaptadores

import android.os.Parcel
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fcfm.adaptadores.R
import kotlinx.android.synthetic.main.producto_custom.view.*

data class Producto(
    val nombre: String,
    val descriptcion: String,
    val categoria: String,
    val color:String
)

class ProductoAdapter (val productos: List<Producto>):
    RecyclerView.Adapter<ProductoAdapter.MiViewHolder>() {

    class MiViewHolder(val miDiseno:View):RecyclerView.ViewHolder(miDiseno){
        fun bind(producto: Producto){
            //Aqui se llena la vista
            miDiseno.tvNombre.text=producto.nombre
            miDiseno.tvCategoria.text=producto.categoria
            miDiseno.tvDescripcion.text=producto.descriptcion
            miDiseno.tvColor.text = producto.color
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MiViewHolder {
       val miDiseno= LayoutInflater.from(parent.context).inflate(R.layout.producto_custom,parent,false)
        return MiViewHolder(miDiseno)
    }

    override fun getItemCount(): Int {
       return productos.size //Aqui mandamos cuantos hará
    }

    override fun onBindViewHolder(holder: MiViewHolder, position: Int) {
        holder.bind(productos[position])
    }



}